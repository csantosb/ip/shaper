-- * Shaper
--
--! @file
--!
--! @brief Input input averaging over a _g_window_ length window
--!
--! Performs an averaging of an arbitrary size input input stream over a time
--! windows of size _2**g_window_, being _g_window_ a generic input to the module.
--!
--! @section avg Averaging
--!
--! Averaging is implemented as the difference of the registered input with its
--! _2**g_window_ shifted version. Result is continuously accumulated to compute a
--! (_2**g_window_ sized) moving window over the input.
--!
--! @section div Division
--!
--! Its output is left bit shifted by _g_window_ to compute division by _2**g_window_.
--!
--! @class averager

-- * Libraries

library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Entity

--! @brief Gets g_window as a generic to compute a 2**g_window length window.
--!
--! @details Rst and clk have standard functionality\n
--! Samples is an arbitrary number of bites input data bus\n\n
--! Average is the sum of all input in the window, divided by its length,
--! thus the average\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
entity shaper is
  generic (g_delay_1 : natural;
           g_delay_2 : natural;
           g_m1      : integer;
           g_m2      : integer;
           g_acc     : natural);
  port (
    --!\name Global signals
    --!\{
    clk    : in  std_logic;
    rst    : in  std_logic;
    --!\}
    --!\name Input and averaged input
    --!\{
    input  : in  signed;
    --! length is 2*(input’length+2)+2*g_acc
    output : out signed
   --!\}
    );
end entity shaper;

-- * Architecture

--! @brief Implements a simple, general purpose moving averaging.
--!
--! @details It delays the incoming sampling signals, sampling them in parallel.\n
--! Then, takes the difference of the two parallel streams\n
--! accumulating the difference.\n
--! Finally, redimensions the results to compute the division by the window lenght
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
architecture modular of shaper is

  -- ** signals

  --! Registered incoming input : input’length+1
  signal delay_1 : signed(input'length downto 0) := (others => '0');

  --! Registered incoming input : input’length+2
  signal delay_2 : signed(delay_1'length downto 0) := (others => '0');

  --! Shifted incoming input : 2*(input’length+2)+g_acc
  signal hpd_1 : signed(2*delay_2'length+g_acc-1 downto 0) := (others => '0');

  --! Registered incoming input : 2*(input’length+2)+g_acc+g_acc
  signal acc : signed(hpd_1'length+g_acc-1 downto 0) := (others => '0');

begin

  u_delay_substract_1 : entity work.delay_substract
    generic map (g_delay => g_delay_1)
    port map (clk    => clk,
              rst    => rst,
              input  => input,
              output => delay_1);

  u_delay_substract_2 : entity work.delay_substract
    generic map (g_delay => g_delay_2)
    port map (clk    => clk,
              rst    => rst,
              input  => delay_1,
              output => delay_2);

  u_hpd : entity work.hpd
    generic map (g_m1  => g_m1,
                 g_m2  => g_m2,
                 g_acc => g_acc)
    port map (clk    => clk,
              rst    => rst,
              input  => delay_2,
              output => hpd_1);

  u_acc : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        acc <= (others => '0');
      else
        acc <= acc + hpd_1;
      end if;
    end if;
  end process;

  --! output size is fixed, that of acc
  output <= acc;

end architecture modular;
